const path = require('path')


module.exports = {
    // entry point 
    entry: './src/index.js',

    // output filename bundle
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist')
    }
}